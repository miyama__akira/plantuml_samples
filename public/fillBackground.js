"use strict";

function init(fillColor = "rgba(255,255,255,1.0)", padding = 4) {
  document.getElementsByTagName("body")[0].style.width = 
    "calc(" + document.getElementsByTagName("svg")[0].style.width + " + 6em)";

  document.querySelectorAll("line + text").forEach(txt => {
    function fval(attr) { return(parseFloat(txt.getAttribute(attr))); }
    const x = fval("x"), y = fval("y"), 
      len = fval("textLength"), fontSize = fval("font-size"), 
      attrib = {x: x - padding, y: y - fontSize - padding, 
        width: len + 2 * padding, height: fontSize + 2 * padding, 
        fill: fillColor}, 
      rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    for (let [key, val] of Object.entries(attrib)) {
      rect.setAttribute(key, val);
    }
    txt.parentNode.insertBefore(rect, txt);
  });
}
