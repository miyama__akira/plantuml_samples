SEQ = FuJian-and-his-subordinates
UML = $(SEQ).uml
SVG = $(SEQ).svg

all: $(SVG)
	mv $(SVG) ../public/05_16/

$(SVG): $(UML)
	plantuml -tsvg $<

.PHONY: clean
clean:
	rm $(DIR)$(SVG)
